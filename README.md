# PostgreSQL Helm chart

![Version: 4.0.0](https://img.shields.io/badge/Version-4.0.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 17.2-alpine](https://img.shields.io/badge/AppVersion-17.2--alpine-informational?style=flat-square)

This repository contains a [Helm](https://helm.sh/) chart to deploy [PostgreSQL](https://www.postgresql.org/) on Kubernetes.
PostgreSQL is a powerful open-source relational database management system that offers robust features and scalability.

We use the official docker image of [PostgreSQL](https://hub.docker.com/_/postgres).

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Felix Stärk | <felix.staerk@posteo.de> |  |
| Sven Prüfer | <sven@musmehl.de> |  |

## Installation and configuration

1. Create namespace.

    ```shell
    kubectl create namespace database
    ```

2. (Optional) Create a secret for the database admin credentials.

    This can be done using a `basic-auth` secret, which allows for the modification of both username and password:

    ```shell
    kubectl create secret generic database-credentials \
    --type=kubernetes.io/basic-auth \
    --from-literal=username=postgres \
    --from-literal=password=CHANGEME \
    --namespace database
    ```

    Alternatively, the secret containing username and password will be randomly generated.
    Note: The password does not need to be base64-encoded beforehand.

3. Adjust the values in the [values.yaml](./values.yaml).

4. Deploy the helm chart

    ```shell
    helm upgrade --install -n database postgresql17 .
    ```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| database.name | string | "postgres" | Name of the database that shall be created on startup (Optional) |
| fullnameOverride | string | `""` | String to fully override postgresql.fullname |
| image.pullPolicy | string | `"IfNotPresent"` | PostgreSQL image pull policy |
| image.repository | string | `"postgres"` | PostgreSQL image repository |
| image.tag | string | Chart.appVersion | PostgreSQL image tag |
| livenessProbe.enabled | bool | `true` | Enable livenessProbe on PostgreSQL containers |
| livenessProbe.failureThreshold | int | `6` | Failure threshold for livenessProbe |
| livenessProbe.initialDelaySeconds | int | `30` | Initial delay seconds for livenessProbe |
| livenessProbe.periodSeconds | int | `10` | Period seconds for livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for livenessProbe |
| livenessProbe.timeoutSeconds | int | `5` | Timeout seconds for livenessProbe |
| nameOverride | string | `""` | String to partially override postgresql.fullname |
| persistence.accessModes | list | `["ReadWriteOnce"]` | PVC Access Mode for PostgreSQL volume |
| persistence.mountPath | string | `"/var/lib/postgresql/data"` | The path the volume will be mounted inside the container |
| persistence.size | string | `"100Gi"` | PVC Storage Request for PostgreSQL volume |
| persistence.storageClassName | string | `"retain-local-path"` | PVC StorageClass for PostgreSQL data volume |
| persistence.subPath | string | `""` | The subdirectory of the volume to mount |
| podManagementPolicy | string | `"OrderedReady"` | Pod management policy for the PostgreSQL statefulset |
| podSecurityContext | object | `{}` | Specify security context for the PostgreSQL pod |
| readinessProbe.enabled | bool | `true` | Enable readinessProbe on PostgreSQL containers |
| readinessProbe.failureThreshold | int | `6` | Failure threshold for readinessProbe |
| readinessProbe.initialDelaySeconds | int | `5` | Initial delay seconds for readinessProbe |
| readinessProbe.periodSeconds | int | `10` | Period seconds for readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for readinessProbe |
| readinessProbe.timeoutSeconds | int | `5` | Timeout seconds for readinessProbe |
| replicaCount | int | `1` | Number of PostgreSQL replicas to deploy |
| resources.limits.memory | string | `"2Gi"` | The memory limits for the PostgreSQL containers |
| resources.requests.cpu | string | `"250m"` | The requested cpu for the PostgreSQL containers |
| resources.requests.memory | string | `"256Mi"` | The requested memory for the PostgreSQL containers |
| securityContext | object | `{}` | Specify security context for individual containers of PostgreSQL pod |
| service.port | int | `5432` | PostgreSQL service port |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for PostgreSQL pod If set to false, the default ServiceAccount is used. |
| serviceAccount.name | string | postgresql.fullname | The name of the service account to use. |
| startupProbe.enabled | bool | `true` | Enable startupProbe on PostgreSQL containers |
| startupProbe.failureThreshold | int | `15` | Failure threshold for startupProbe |
| startupProbe.initialDelaySeconds | int | `30` | Initial delay seconds for startupProbe |
| startupProbe.periodSeconds | int | `10` | Period seconds for startupProbe |
| startupProbe.successThreshold | int | `1` | Success threshold for startupProbe |
| startupProbe.timeoutSeconds | int | `1` | Timeout seconds for startupProbe |
| terminationGracePeriodSeconds | string | `""` | Seconds PostgreSQL pod needs to terminate gracefully |
| updateStrategy.rollingUpdate | object | `{}` | PostgreSQL statefulset rolling update configuration parameters |
| updateStrategy.type | string | `"RollingUpdate"` | PostgreSQL statefulset update strategy type |
| user.existingSecret | string | `"database-credentials"` | Name of the `basic-auth` secret to use (Optional) |
| user.name | string | "postgres" | Username of the database user (Optional). The username specified in the existing secret will take precedence. |
| volumeMounts | list | `[]` | Additional volumeMounts (Optional) |
| volumes | list | `[]` | Additional volumes to mount (Optional) |

## Migration guide

To upgrade minor version, just update the image tag. Upgrading major versions need manual steps.

1. Deploy the new version.

    ```shell
    helm upgrade --install -n database "postgresql${NEW_VERSION}" .
    ```

2. Dump the old database.

    ```shell
    pg_dumpall -U postgres > database.sql
    ```

3. Import the data to the new database.

    ```shell
    psql -v postgres < database.sql
    ```

4. For performance reasons, run the following sql command for any database.

    ```sql
    ANALYZE VERBOSE;
    ```

5. Delete the old release.

    ```shell
    helm uninstall -n database "postgresql${OLD_VERSION}"
    kubectl delete -n database pvc "${PVC_NAME}"
    kubectl delete -n database pv "${PV_NAME}"
    ```

    Maybe, you also need to manually delete the data from the actual storage endpoint.

## License

This repository is based on the work from the [Bitnami PostgreSQL helm chart](https://github.com/bitnami/charts/tree/main/bitnami/postgresql) which is licensed under the [Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
The modifications and contents of this repository are licensed under [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0), see [LICENSE](LICENSE).

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.14.2](https://github.com/norwoodj/helm-docs/releases/v1.14.2)
